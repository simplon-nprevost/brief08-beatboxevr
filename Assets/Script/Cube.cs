using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cube : MonoBehaviour
{
    
        public float speed = 10.0f;
        private Rigidbody _rb;
        private Vector2 _screenBounds;
        [SerializeField]
        private GameObject _boundry;
    


        // Use this for initialization
        void Start()
        {
            _rb = this.GetComponent<Rigidbody>();

            _rb.velocity = new Vector2(-speed, 0);
            _screenBounds = Camera.main.ScreenToWorldPoint(new Vector3(1, 1, Camera.main.transform.position.z));
        Debug.Log("cubes move ");

    }

        // Update is called once per frame
        void Update()
        {
            if (transform.position.x < _screenBounds.x * 2)
            {
            Debug.Log("cubes  destroy");
                Destroy(this.gameObject);
            }
        }
    private void OnTriggerEnter(Collider other)
    {
        var bond = _boundry.GetComponent<BoxCollider>();
        if (bond.enabled) {

            Destroy(this.gameObject);
        }

    }
}

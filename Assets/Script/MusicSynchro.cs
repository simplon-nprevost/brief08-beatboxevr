using UnityEngine;

public class MusicSynchro : MonoBehaviour
{
    public GameObject[] cubes;
    public Transform[] points;
    public float beat = (60 / 130) * 2;
    private float _timer;

    private void Update()
    {
        if (_timer > beat)
        {
            GameObject cube = Instantiate(cubes[Random.Range(0, 2)], points[Random.Range(0, 4)]);
            cube.transform.localPosition = Vector3.zero;
            cube.transform.Rotate(transform.forward, 90 * Random.Range(0, 4));
            _timer -= beat;
        }
        _timer += Time.deltaTime;
    }
}

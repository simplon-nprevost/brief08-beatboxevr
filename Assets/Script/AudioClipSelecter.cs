using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[Serializable]
struct AudioLevel
{
    public AudioClip clip;
    public float bpm;
}
public class AudioClipSelecter : MonoBehaviour
{
    [SerializeField] private List<AudioLevel> _levels = new List<AudioLevel>();
    [SerializeField] private AudioSource _audio;
    [SerializeField] private TMP_Text _bpmDisplay;
    [SerializeField] private TMP_Text _durationDisplay;

    void Start()
    {
        SetAudioClip(0);
    }
    void Update()
    {
        Debug.Log(_levels.Count);
    }

    public void SetAudioClip(int num)
    {
        _audio.clip = _levels[num].clip;
        _bpmDisplay.text = "BPM : " + _levels[num].bpm.ToString();
        _durationDisplay.text = "Duration : " + System.Convert.ToInt32(_levels[num].clip.length / 60).ToString() + " min " + System.Convert.ToInt32(_levels[num].clip.length % 60).ToString() + " sec";
        GameManager.beat = 60.0f / _levels[num].bpm;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ToDodge : Object
{
    [SerializeField] private int score = 1;

    public override void Bonus(int score)
    {
        base.Bonus(score);
    }    
    
    public override void Good(int score)
    {
        base.Good(score);
    }    
    
    public override void Miss()
    {
        base.Miss();
        scoreManager.ModifyMultiplicateur(-2);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "ObjectDisabler")
        {
            Miss();
        }
    }
}

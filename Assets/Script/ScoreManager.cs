using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;

public class ScoreManager : MonoBehaviour
{
    private int _multiplicator = 1;
    public static int Score { private set; get; }
    [SerializeField] private TMP_Text _scoreDisplay;
    [SerializeField] private TMP_Text _multiplicatorDisplay;
    private int _enchainedMissCount;
    public UnityEvent Defeat;
    public delegate void DefeatAction();
    public static event DefeatAction OnDefeat;
    private bool _isGodmodEnabled = false;

    public void AddScore(int points)
    {
        Score += points * _multiplicator;
        _scoreDisplay.text = Score.ToString();
        _enchainedMissCount = 0;
    }

    public void ModifyMultiplicateur(int changeValue)
    {
        _multiplicator += changeValue;
        if(_multiplicator < 1)
        {
            _multiplicator = 1;
        }
        _multiplicatorDisplay.text = _multiplicator.ToString() + "X";
    }

    public void MissDetected()
    {
        if(_multiplicator == 1 && _isGodmodEnabled == false)
        {
            _enchainedMissCount++;
            if(_enchainedMissCount >= 5)
            {
                Defeat.Invoke();
                OnDefeat();
            }
        }
    }

    public void SetGodmod(bool godmod)
    {
        _isGodmodEnabled = godmod;
    }
}

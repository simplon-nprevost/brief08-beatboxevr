using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;

public class CountdownManager : MonoBehaviour
{
    [SerializeField] private TMP_Text _coutdownText;
    private int _coutdownTime = 3;
    [SerializeField] private GameObject _countdownCanva;
    private static CountdownManager Instance;
    public UnityEvent AudioStarted;

    void Start()
    {
        Instance = this;
    }

    public static void Countdown()
    {
        GameManager.IsOnPlay = false;
        Instance.StartCoroutine(Instance.CountdownDisplay());
    }

    IEnumerator CountdownDisplay()
    {
        float counter = 0.0f;
        _coutdownTime = 3;
        _coutdownText.text = _coutdownTime.ToString();
        _countdownCanva.SetActive(true);
        while (_coutdownTime > 0)
        {
            while (counter < 1)
            {
                counter += Time.deltaTime;
                yield return null;
            }
            _coutdownTime--;
            _coutdownText.text = _coutdownTime.ToString();
            counter = 0.0f;
        }
        
        _countdownCanva.SetActive(false);
        GameManager.IsOnPlay = true;
        if (GameManager.isFromPause == false)
        {
            Instance.StartCoroutine(Instance.AudioDelay());
        }
        else
        {
            GameManager.audio.Play();
        }
    }

    IEnumerator AudioDelay()
    {
        float delay = 3.2f;
        while (delay > 0)
        {
            delay -= Time.deltaTime;
            yield return null;
        }
        GameManager.audio.Play();
        AudioStarted.Invoke();
    }
}

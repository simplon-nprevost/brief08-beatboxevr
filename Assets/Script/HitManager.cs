using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class HitManager : MonoBehaviour
{
    public UnityEvent hitDetected;
    public UnityEvent precisionHitDetected;
    public UnityEvent wrongHand;
    [SerializeField] private Renderer _targetRenderer;


    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Hands"))
        {
            if(_targetRenderer.material.color == other.gameObject.GetComponent<Renderer>().material.color)
            {
                if (checkPrecision(other) == true)
                {
                    precisionHitDetected.Invoke();
                }
                else
                {
                    hitDetected.Invoke();
                }
            }
            else
            {
                wrongHand.Invoke();
            }
        }
    }

    private bool checkPrecision(Collider gloveCollider)
    {
        Collider objectCollider = GetComponent<Collider>();
        Vector3 targetCenter = objectCollider.bounds.center;
        Vector3 closestPoint = gloveCollider.ClosestPoint(targetCenter);
        float distance = Vector3.Distance(targetCenter, closestPoint);

        if(distance < 2.0f)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}

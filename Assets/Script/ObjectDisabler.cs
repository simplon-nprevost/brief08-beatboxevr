using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectDisabler : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("cube") == true)
        {
            if (other.gameObject.GetComponent<Object>().IsMissed == false)
            {
                other.gameObject.GetComponent<Object>().Miss();
            }
            else if (other.gameObject.GetComponent<ToDodge>() != null)
            {
                other.gameObject.GetComponent<ToDodge>().Good(1);
            }
            else
            {
                other.gameObject.SetActive(false);
            }
        }
    }    
}

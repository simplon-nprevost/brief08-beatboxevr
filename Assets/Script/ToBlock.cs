using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToBlock : Object
{
    [SerializeField] private int score = 1;

    void OnTriggerEnter(Collider other)
    {
        if(GuardManager._isPlayerOnGuardPosition == true)
        {
            Good(score);
        }
        else
        {
            Miss();
        }
    }

    public override void Bonus(int score)
    {
        base.Bonus(score);
    }

    public override void Good(int score)
    {
        base.Good(score);
    }

    public override void Miss()
    {
        base.Miss();
        scoreManager.ModifyMultiplicateur(-1);
    }
}

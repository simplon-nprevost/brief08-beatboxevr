using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToHit : Object
{
    [SerializeField] private int score = 1;


    public override void Bonus(int score)
    {
        base.Bonus(score);
        scoreManager.ModifyMultiplicateur(2);
    }

    public override void Good(int score)
    {
        base.Good(score);
        scoreManager.ModifyMultiplicateur(1);
    }

    public override void Miss()
    {
        base.Miss();
        scoreManager.ModifyMultiplicateur(-1);

    }
}

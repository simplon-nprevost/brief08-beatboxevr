using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class WrongMoveDetection : MonoBehaviour
{
    public UnityEvent wrong;

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Hands"))
        {
            wrong.Invoke();
        }
    }
}

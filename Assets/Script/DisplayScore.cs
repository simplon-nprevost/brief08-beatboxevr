using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DisplayScore : MonoBehaviour
{
    [SerializeField] private TMP_Text _scoreDisplay;
    // Start is called before the first frame update
    void OnEnable()
    {
        _scoreDisplay.text += ScoreManager.Score.ToString();
    }
}


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuardManager : MonoBehaviour
{
    public static bool _isPlayerOnGuardPosition { get; private set; }
    [SerializeField] private Transform _leftHand;
    [SerializeField] private Transform _rightHand;
    [SerializeField] private Transform _head;
    [SerializeField] private GameObject _shield;

    void Start()
    {
        _isPlayerOnGuardPosition = false;
    }

    void Update()
    {
        if((Vector3.Distance(_leftHand.position, _rightHand.position) < 0.2f) && (Vector3.Distance(_leftHand.position, _head.position) < 0.265f))
        {
            if (Time.fixedTime >= 1.0f)
            {
                _isPlayerOnGuardPosition = true;
                _shield.SetActive(true);
            }
        }
        else
        {
            _isPlayerOnGuardPosition = false;
            _shield.SetActive(false);
        }
    }

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;

public class GameManager : MonoBehaviour
{
    [SerializeField] private GameObject[] _entityPrefabs; // Les prefabs des 3 entit�s � spawner
    [SerializeField] private int _poolSize = 35; // La taille du pool d'entit�s
    [SerializeField] private Transform[] _spawnPositions; // Les positions de spawn
    [SerializeField] private Transform _playerHead;
    private List<GameObject>[] _entityPools; // Les listes des entit�s dans les diff�rents pools
    private bool _isSpawnHeightSet = false;
    public static bool IsOnPlay = false;
    public static bool IsOnPlayMode { get; private set; }
    public UnityEvent PlayMode;
    public UnityEvent MenuMode;
    public UnityEvent LevelEnded;
    public static float beat = (60.0f / 105.0f);
    private float _timer = 0;
    public static AudioSource audio;
    [SerializeField] private GameObject _winMenu;
    public static bool isFromPause = false;
    public delegate void WinAction();
    public static event WinAction OnWin;

    private void Start()
    {
        audio = GameObject.Find("audio").GetComponent<AudioSource>();
        // Initialisation des pools d'entit�s
        _entityPools = new List<GameObject>[_entityPrefabs.Length];
        for (int i = 0; i < _entityPrefabs.Length; i++)
        {
            _entityPools[i] = new List<GameObject>();
            for (int j = 0; j < _poolSize; j++)
            {
                GameObject entity = Instantiate(_entityPrefabs[i]);
                entity.SetActive(false);
                _entityPools[i].Add(entity);
            }
        }
    }

    void Update()
    {
        if (IsOnPlay == true && _timer > beat)
        {
            if (_isSpawnHeightSet == false)
            {
                for (int i = 0; i < 3; i++)
                {
                    _spawnPositions[i].position = new Vector3(_spawnPositions[i].position.x, _playerHead.position.y, _spawnPositions[i].position.z);
                }
                _isSpawnHeightSet = true;
            }
            EntitySpawn();
            _timer = 0;
        }
        _timer += Time.deltaTime;
    }

    void EntitySpawn()
    {
        // Spawn d'une entit� al�atoire � une position al�atoire
        int randomIndex = Random.Range(0, _entityPrefabs.Length);
        int randomPositionIndex = Random.Range(0, _spawnPositions.Length);
        GameObject entity = GetEntityFromPool(randomIndex);
        if (entity != null)
        {
            entity.transform.position = _spawnPositions[randomPositionIndex].position;
            entity.SetActive(true);
        }
    }

    private GameObject GetEntityFromPool(int entityIndex)
    {
        // Chercher une entit� active dans le pool correspondant
        for (int i = 0; i < _entityPools[entityIndex].Count; i++)
        {
            if (!_entityPools[entityIndex][i].activeInHierarchy)
            {
                return _entityPools[entityIndex][i];
            }
        }
        return null;
    }



    public void SetPlayerToPlayPos(GameObject player)
    {
        PlayMode.Invoke();
        player.transform.position = new Vector3(-5.7f, -4.3f, 8.1f);
        player.transform.rotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);
        CountdownManager.Countdown();
    }

    public void SetPlayerToMainMenuPos(GameObject player)
    {
        MenuMode.Invoke();
        player.transform.position = new Vector3(-5.9f, -4.3f, 7.6f);
        player.transform.rotation = Quaternion.Euler(0.0f, 180.0f, 0.0f);
    }

    public static void TogglePLayMode(bool value)
    {
        IsOnPlayMode = value;
    }

    public void StartEndAudioTracking()
    {
        StartCoroutine(WaitForAudioEnded(audio.clip.length));
    }

    IEnumerator WaitForAudioEnded(float time)
    {
        float delay = 0.0f;
        while (delay < time)
        {
            if(delay >= time - 3.4f)
            {
                IsOnPlay = false;
            }
            delay += Time.deltaTime;
            yield return null;
        }
        MenuMode.Invoke();
        LevelEnded.Invoke();
        OnWin();
    }

    public void Defeated()
    {
        IsOnPlay = false;
        audio.Stop();
        MenuMode.Invoke();
    }
}

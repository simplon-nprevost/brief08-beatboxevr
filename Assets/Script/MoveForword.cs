using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[AddComponentMenu("Movement/Move Forward")]
public class MoveForword : MonoBehaviour
{
    public float speed = 30.0f;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.IsOnPlay == true)
        {
            transform.position += -transform.forward * 5 * Time.deltaTime;
        }
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PauseManager : MonoBehaviour
{
    bool isPaused = false;
    public delegate void ResetAction();
    public static event ResetAction OnReset;
    public UnityEvent ReturnToGame;
    public UnityEvent ReturnToMainMenu;

    public void TogglePause()
    {
        if (isPaused == false)
        {
            isPaused = true;
            GameManager.IsOnPlay = false;
            Time.timeScale = 0;
        }
        else if (isPaused == true)
        {
            isPaused = false;
            GameManager.isFromPause = true;
            Time.timeScale = 1;
            ReturnToGame.Invoke();
        }
    }

    public void ResetLevel()
    {
        isPaused = false;
        Time.timeScale = 1;
        OnReset();
    }

    public void GoToMainMenu()
    {
        Time.timeScale = 1;
        isPaused = false;
        GameManager.isFromPause = false;
        GameManager.TogglePLayMode(false);
        ReturnToMainMenu.Invoke();
        OnReset();
    }
}

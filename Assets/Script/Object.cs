using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;

public class Object : MonoBehaviour
{
    protected static ScoreManager scoreManager;
    public bool IsMissed { get; private set; }

    void Start()
    {
        if (scoreManager == null)
        {
            scoreManager = GameObject.Find("ScoreManager").GetComponent<ScoreManager>();
        }
        PauseManager.OnReset += Reset;
        ScoreManager.OnDefeat += DisableObject;
        GameManager.OnWin += DisableObject;
    }

    public virtual void Miss()
    {
        scoreManager.MissDetected();
        IsMissed = true;
    }

    public virtual void Good(int objectScore)
    {
        scoreManager.AddScore(objectScore);
        DisableObject();
    }

    public virtual void Bonus(int objectScore)
    {
        scoreManager.AddScore(objectScore);
        DisableObject();
    }

    private void Reset()
    {
        DisableObject();
    }

    private void DisableObject()
    {
        gameObject.SetActive(false);
    }
}
